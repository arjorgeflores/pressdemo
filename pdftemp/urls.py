from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'pdftemp'
urlpatterns = [
    path('', views.index, name='index'),
    path('template/', views.template, name='template'),
    path('htmltopdf/', views.htmltopdf, name='htmltopdf'),
    path('pdftopdf/', views.pdftopdf, name='pdftopdf'),
    path('pdffile/', views.pdffile, name='pdffile')
]
