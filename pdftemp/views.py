from django.shortcuts import render
from pprint import pprint
from django.http import HttpResponse, FileResponse, Http404
from django.views.decorators.clickjacking import xframe_options_exempt
from pdfjinja import PdfJinja
import os
from django.conf import settings
import time
import pdfkit

WKHTMLTOPDF_PATH = '/usr/bin/wkhtmltopdf'

# Create your views here.
def  index(request):
    #latest_question_list = Question.objects.order_by('-pub_date')[:5]
    pprint(request.GET.get('name'))
    context = {
        'latest_question_list': 'test',
    }
    return render(request, 'pdftemp/index.html', context)

@xframe_options_exempt
def  template(request):
    #latest_question_list = Question.objects.order_by('-pub_date')[:5]
    #pprint(request.GET.get('name'))
    context = {
        'new_model': 'New 2019 Jeep Wrangler Sport',
        'dealership_logo': 'http://i.hmp.me/m/38ddf3f9c29c3c2d064a46cf697aea29.png',
        'car_img': 'http://i.hmp.me/m/814e5f3972fc73d6223af340b6b90915.png',
        'price_title': 'Your New Payment Estimate',
        'price': '$240.99',
        'payment_frecuency': 'Bi-weekly',
        'new_payment_line1': '2 New Payment Estimate:',
        'new_payment_line2': 'with trade in (monthly)',
        'price2': '$458.00',
    }
    """
    context = {
        'new_model': 'New 2020 Grand Cherokee Sport',
        'dealership_logo': 'http://i.hmp.me/m/38ddf3f9c29c3c2d064a46cf697aea29.png',
        'car_img': 'http://i.hmp.me/m/2f00be434664f04a35527df1c8d0db3a.png',
        'price_title': 'Your New Payment Estimate',
        'price': '$333.99',
        'payment_frecuency': 'Bi-weekly',
        'new_payment_line1': '2 New Payment Estimate:',
        'new_payment_line2': 'with trade in (monthly)',
        'price2': '$558.00',
    }
    """

    return render(request, 'pdftemp/template.html', context)


def htmltopdf(request):
    # Create a URL of our project and go to the template route
    projectUrl = request.get_host() + '/pdftemp/template'
    options = {
        'page-size': 'Letter',
        'margin-top': '2.1mm',
        'margin-right': '2.1mm',
        'margin-bottom': '2.1mm',
        'margin-left': '2.1mm',
        'encoding': 'UTF-8',
        'dpi': 1200
    }

    #config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)
    pdf = pdfkit.from_url(projectUrl, False)
    # Generate download
    response = HttpResponse(pdf,content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="htmltopdf.pdf"'

    return response


def pdftopdf(request):

    pdfjinja = PdfJinja('static/example.pdf')
    context = {
      "new_model" : "New 2021 Mercedes-Benz E 300 4MATIC Sedan",
      "dealership_logo": "static/mercedesBenzLogo.png",
      "car_img" : "static/mbcar.png",
      "price_title": "Your New Payment Estimate",
      "price": "$471.99",
      "payment_frecuency": "Bi-weekly",
      "new_payment_line1": "2 New Payment Estimate:",
      "new_payment_line2": "with trade in (monthly)",
      "price2": "$458.00",
      "code": "HT67RJJ43",
      "page": "coco.com"
    }

    context = {
      "new_model" : "New 2020 Audi A4 Sedan",
      "dealership_logo": "static/audilogo.png",
      "car_img" : "static/audia4.png",
      "price_title": "Your New Payment Estimate",
      "price": "$471.99",
      "payment_frecuency": "Bi-weekly",
      "new_payment_line1": "2 New Payment Estimate:",
      "new_payment_line2": "with trade in (monthly)",
      "price2": "$458.00",
      "code": "HT67RJJ43",
      "page": "coco.com"
    }

    pdfout = pdfjinja(context)
    path_to_file='static/mercedez.pdf'
    pdf = pdfout.write(open(path_to_file, 'wb'))

    time.sleep(3)

    file_path = os.path.join(settings.MEDIA_ROOT, path_to_file)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'attachment; filename="pdftopdf.pdf"'
            #response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


@xframe_options_exempt
def pdffile(request):
    try:
        return FileResponse(open('static/example.pdf', 'rb'), content_type='application/pdf')
    except FileNotFoundError:
        raise Http404('not found')
