from django.apps import AppConfig


class PdftempConfig(AppConfig):
    name = 'pdftemp'
