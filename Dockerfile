FROM python:3.7

# Copy default endpoint specific user settings overrides into container to specify Python path
COPY .devcontainer/settings.vscode.json /root/.vscode-remote/data/Machine/settings.json

ENV PYTHONUNBUFFERED 1
ENV DEBUG True
ENV SECRET_KEY asdfasdf
ENV DATABASE_URL postgres://postgres:postgres@service_phones_db/ar_services_printing
ENV ALLOWED_HOSTS 0.0.0.0,localhost
ENV AWS_ACCESS_KEY_ID asdf
ENV AWS_SECRET_ACCESS_KEY asdf

RUN mkdir /code
COPY . /code/
WORKDIR /code/

RUN pip install pipenv
RUN pipenv install --dev
RUN echo 'SHELL=/bin/bash pipenv shell' >> /root/.bashrc

EXPOSE 8012

ENV SHELL /bin/bash
